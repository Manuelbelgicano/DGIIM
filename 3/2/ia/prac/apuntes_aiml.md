# Apuntes AIML

Sintaxis para reglas:
```xml
<category>
<pattern> Patrón de la regla </pattern>
<template> Respuesta </template>
</category>
```

### Comodines

 - **Comodín `*`:** Sustituye una o más palabras después de `Patrón`.
```xml
<pattern> Patrón * </pattern>
```
 - **Comodín `_`:** Equivalente a `*`.
```xml
<pattern> Patrón _ </pattern>
```
 - **Comodín `^`:** Sustituye cero o más palabras después de `Patrón`.
```xml
<pattern> Patrón ^ </pattern>
```
 - **Comodín `#`:** Equivalente a `^`.
```xml
<pattern> Patrón # </pattern>
```
El orden de prioridades para estos comodines es el siguiente:
`#` > `_` > `Patrón específico` > `^` > `*`

 - **Comodín `$`:** Hace que el patrón específico con la palabra precedida por el comodín sea
la regla con mayor prioridad.
```xml
<pattern> $Palabra patrón </pattern>
```

### Uso de comodines en la respuesta

Podemos hacer que el bot responda con lo que ha sido capturado por el comodín escribiendo `<star/>`.
En caso de que haya más de un comodín, debemos especificar su número cardinal en el patrón.
Por ejemplo, si queremos que responda con el segundo comodín:
```xml
<category>
<pattern> Palabra * palabra * </pattern>
<template> Respuesta <star index="2"/> </template>
</category>
```