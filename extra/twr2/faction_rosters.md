# Total War: Rome II faction rosters

### ARDIAEI: 11 UNITS

- Melee Infantry: 2
	- Illyrian Levies
	- Illyrian Raiders

- Spear Infantry: 6
	- Illyrian Tribesmen
	- Illyrian Spearmen
	- Illyrian Thureos Spears
	- Illyrian Marines
	- Illyrian Hoplites
	- Illyrian Noble Hoplites

- Missile Infantry: 2
	- Slave Javelinmen
	- Slave Slingers

- Melee Cavalry: 1
	- Illyrian Cavalry

### AREVACI: 15 UNITS

- Melee Infantry: 5
	- Iberian Swordsmen
	- Guerrilla Warriors
	- Painted Warriors
	- Scutarii
	- Noble Fighters

- Spear Infantry: 3
	- Iberian Tribesmen
	- Iberian Spearmen
	- Scutarii Spearmen

- Missile Infantry: 3
	- Iberian Skirmishers
	- Iberian Slingers
	- Balearic Slingers

- Melee Cavalry: 3
	- Iberian Cavalry
	- Celtiberian Cavalry
	- Noble Cavalry

- Missile Cavalry: 1
	- Cantabrian Cavalry

#### ARMENIA: 20 UNITS

- Melee Infantry: 4
	- Mob
	- Hillmen
	- Axemen
	- Kartli Axemen

- Spear Infantry: 3
	- Eastern Spearmen
	- Persian Hoplites
	- Noble Spearmen

- Missile Infantry: 4
	- Eastern Javelinmen
	- Eastern Slingers
	- Eastern Archers
	- Elite Persian Archers

- Melee Cavalry: 2
	- Noble Blood Cavalry
	- Azat Knights

- Shock Cavalry: 3
	- Persian Cavalry
	- Eastern Cataphracts
	- Royal Cataphracts

- Missile Cavalry: 4
	- Horse Skirmishers
	- Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers

### ARVERNI: 16 UNITS

- Melee Infantry: 4
	- Celtic Warriors
	- Naked Warriors
	- Chosen Swordsmen
	- Oathsworn

- Spear Infantry: 5
	- Celtic Tribesmen
	- Levy Freemen
	- Spear Warriors
	- Chosen Spearmen
	- Spear Nobles

- Missile Infantry: 4
	- Celtic Youths
	- Celtic Slingers
	- Celtic Skirmishers
	- Gallic Hunters

- Melee Cavalry: 3
	- Light Horse
	- Heavy Horse
	- Noble Horse

###	ATHENS: 19 UNITS

- Melee Infantry: 2
	- Mob
	- Thorax Swordsmen

- Spear Infantry: 8
	- Citizen Hoplites
	- Militia Hoplites
	- Light Hoplites
	- Thureos Spears
	- Thureos Hoplites
	- Hoplites
	- Thorax Hoplites
	- Picked Hoplites

- Pike Infantry: 1
	- Pikemen

- Missile Infantry: 4 (5)
	- Javelinmen
	- Slingers
	- Archers
	- (Light) Peltasts

- Melee Cavalry: 1
	- Citizen Cavalry

- Shock Cavalry: 1
	- Hippeus Lancers

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry

###	BAKTRIA: 26 UNITS

- Melee Infantry: 3
	- Mob
	- Bactrian Hillmen
	- Thorax Swordsmen

- Spear Infantry: 4
	- Eastern Spearmen
	- Thureos Spears
	- Scale Thorax Hoplites
	- Bactrian Royal Guard

- Pike Infantry: 3
	- Levy Pikemen
	- Pikemen
	- Thorax Pikemen

- Missile Infantry: 6 (7)
	- Eastern Javelinmen
	- Eastern Spearmen
	- Persian Light Archers
	- (Light) Peltasts
	- Elite Persian Archers
	- Bactrian Peltasts

- Melee Cavalry: 3
	- Bactrian Light Horse
	- Citizen Cavalry
	- Bactrian Noble Horse

- Shock Cavalry: 2
	- Bactrian Royal Cavalry
	- Hellenic Cataphracts

- Missile Cavalry: 3
	- Horse Skirmishers
	- Horse Archers
	- Balearic Horse Archers

- Elephant: 2
	- Indian War Elephants
	- Indian Armoured Elephants

###	BOII: 15 UNITS

- Melee Infantry: 5
	- Celtic Warriors
	- Axe Warriors
	- Naked Warriors
	- Sword Followers
	- Oathsworn

- Spear Infantry: 4
	- Celtic Tribesmen
	- Levy Freemen
	- Spear Warriors
	- Veteran Spears

- Missile Infantry: 3
	- Celtic Slingers
	- Celtic Bowmen
	- Celtic Skirmishers

- Melee Cavalry: 3
	- Light Horse
	- Heavy Horse
	- Noble Horse

###	CARTHAGE: 13 UNITS

- Melee Infantry: 2
	- Mob
	- Libyan Infantry

- Spear Infantry: 4 (6)
	- Citizen Militia
	- (Late) Libyan Hoplites
	- (Late) Carthaginian Hoplites
	- Sacred Band

- Pike Infantry: 1
	- African Pikemen

- Missile Infantry: 2
	- Libyan Javelinmen
	- Libyan Peltasts

- Melee Cavalry: 1
	- Carthaginian Cavalry

- Shock Cavalry: 1
	- Noble Cavalry

- Elephant: 2
	- African Elephants
	- African War Elephants

### CIMMERIA: 18 UNITS

- Melee Infantry: 1
	- Young Axes

- Spear Infantry: 7
	- Tribesmen
	- Steppe Spearmen
	- Citizen Hoplites
	- Militia Hoplites
	- Scythian Hoplites
	- Hoplites
	- Cimmerian Noble Infantry

- Missile Infantry: 5
	- Javelinmen
	- Slingers
	- Steppe Archers
	- Cimmerian Heavy Archers
	- Picked Peltasts

- Melee Cavalry: 1
	- Citizen Cavalry

- Shock Cavalry: 1
	- Steppe Lancers

- Missile Cavalry: 3
	- Steppe Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers

###	COLCHIS: 16 UNITS

- Melee Infantry: 4
	- Mob
	- Hillmen
	- Axemen
	- Kartli Axemen

- Spear Infantry: 3
	- Eastern Spearmen
	- Hoplites
	- Colchian Nobles

- Missile Infantry: 4
	- Eastern Javelinmen
	- Eastern Slingers
	- Eastern Archers
	- Peltasts

- Melee Cavalry: 2
	- Citizen Cavalry
	- Noble Blood Cavalry

- Shock Cavalry: 1
	- Hippeus Lancers

- Missile Cavalry: 2
	- Horse Skirmishers
	- Horse Archers

### EGYPT: 32 UNITS

- Melee Infantry: 9
	- Mob
	- Egyptian Infantry
	- Karian Axemen
	- Galatian Swordsmen
	- Sobek Cultists
	- Thorax Swordsmen
	- Royal Thorax Swordsmen
	- Royal Peltasts
	- Galatian Royal Guard

- Spear Infantry: 3
	- Levy Thureos Spears
	- Thureos Spears
	- Nubian Spearmen

- Pike Infantry: 4
	- Egyptian Pikemen
	- Pikemen
	- Thorax Pikemen
	- Hellenic Royal Guard

- Missile Infantry: 5 (6)
	- Egyptian Javelinmen
	- Egyptian Slingers
	- Egyptian Archers
	- Nubian Archers
	- (Light) Peltasts

- Melee Cavalry: 4
	- Camel Spearmen
	- Light Cavalry
	- Citizen Cavalry
	- Egyptian Cavalry

- Shock Cavalry: 1
	- Ptolemaic Cavalry

- Missile Cavalry: 3
	- Skirmisher Cavalry
	- Camel Archers
	- Tarantine Cavalry

- Elephant: 2
	- African Elephants
	- African War Elephants

- Chariot: 1
	- Scythed Chariots

### EPIRUS: 21 UNITS

- Melee Infantry: 3
	- Mob
	- Illyrian Levies
	- Royal Peltasts

- Spear Infantry: 4
	- Citizen Hoplites
	- Milia Hoplites
	- Thureos Spears
	- Hoplites

- Pike Infantry: 3
	- Levy Pikemen
	- Pikemen
	- Hellenic Royal Guard

- Missile Infantry: 5
	- Javelinmen
	- Slingers
	- Archers
	- Agrianian Axemen
	- Peltasts

- Melee Cavalry: 2
	- Citizen Cavalry
	- Aspis Companion Cavalry

- Shock Cavalry: 2
	- Thessalian Cavalry
	- Hellenic Royal Cavalry

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry

### GALATIA: 12 UNITS

- Melee Infantry: 3
	- Galatian Swords
	- Naked Swords
	- Galatian Legionaries

- Spear Infantry: 4
	- Celtic Tribesmen
	- Levy Freemen
	- Galatian Spears
	- Galatian Noblemen

- Missile Infantry: 2
	- Celtic Slingers
	- Celtic Skirmishers

- Melee Cavalry: 2
	- Light Horse
	- Noble Horse

- Missile Cavalry: 1
	- Galatian Raiders

### GETAE: 15 UNITS

- Melee Infantry: 2
	- Falxmen
	- Noble Swords

- Spear Infantry: 6
	- Dacian Tribesmen
	- Spears
	- Spear Warriors
	- Heavy Spears
	- Armoured Spears
	- Noble Spears

- Missile Infantry: 4
	- Dacian Bowmen
	- Dacian Skirmishers
	- Dacian Heavy Bowmen
	- Dacian Heavy Skirmishers

- Melee Cavalry: 1
	- Spear Horsemen

- Shock Cavalry: 1
	- Noble Horsemen

- Missile Cavalry: 1
	- Bow Horsemen

### ICENI: 16 UNITS

- Melee Infantry: 6
	- Sword Band
	- Ambushers
	- Painted Ones
	- Druidic Nobles
	- Chosen Sword Band
	- Heroic Nobles

- Spear Infantry: 4
	- Farmers
	- Levy Freemen
	- Spear Band
	- Chosen Spear Band

- Missile Infantry: 2
	- Briton Skirmishers
	- Briton Slingers

- Melee Cavalry: 3
	- Briton Scout Riders
	- Veteran Riders
	- Heroic Riders

- Chariot: 1
	- Chariots

###  KUSH: 24 UNITS

- Melee Infantry: 6
	- Slave Infantry
	- Kushite Slave Infantry
	- Swordsmen
	- Disciples of Apedemak
	- Shotel Warriors
	- Armoured Shotel Warriors

- Spear Infantry: 3
	- Kushite Slave Spearmen
	- Nubian Spearmen
	- Leopard Warriors

- Pike Infantry: 1
	- Kushite Pikes

- Missile Infantry: 6
	- Tribesmen
	- Slave Slingers
	- Archers
	- Nubian Bowmen
	- Kushite Archers
	- Royal Kushite Archers

- Melee Cavalry: 4
	- Desert Cavalry
	- Aethiopian Cavalry
	- Armoured Desert Cavalry
	- Kushite Royal Guard

- Elephant: 2
	- African Elephants
	- African War Elephants

- Chariot: 2
	- Desert Chariots
	- Scythed Chariots

### LUSITANI: 15 UNITS

- Melee Infantry: 5
	- Iberian Swordsmen
	- Iberian Swordswomen
	- Guerrilla Warriors
	- Veteran Shield Warriors
	- Lusitani Nobles

- Spear Infantry: 4
	- Iberian Tribesmen
	- Lusitani Guerrillas
	- Scutarii Spearmen
	- Lusitani Spearmen

- Missile Infantry: 3
	- Iberian Skirmishers
	- Iberian Slingers
	- Balearic Slingers

- Melee Cavalry: 2
	- Iberian Cavalry
	- Scutarii Cavalry

- Missile Cavalry: 1
	- Cantabrian Cavalry

### MACEDON: 23 UNITS

- Melee Infantry: 3
	- Mob
	- Thorax Swordsmen
	- Royal Peltasts

- Spear Infantry: 4
	- Militia Hoplites
	- Thureos Spears
	- Hoplites
	- Shield Bearers

- Pike Infantry: 4
	- Levy Pikemen
	- Pikemen
	- Thorax Pikemen
	- Foot Companions

- Missile Infantry: 5 (6)
	- Javelinmen
	- Slingers
	- Archers
	- (Light) Peltasts
	- Agrianian Axemen

- Melee Cavalry: 2
	- Citizen Cavalry
	- Aspis Companion Cavalry

- Shock Cavalry: 3
	- Sarissa Cavalry
	- Thessalian Cavalry
	- Companion Cavalry

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry

### MASAESYLI: 25 UNITS

- Melee Infantry: 5
	- Slave Infantry
	- Numidian Light Infantry
	- Gaetuli Tribesmen
	- Desert Cohort
	- Desert Legionaries

- Spear Infantry: 4
	- Numidian Spearmen
	- Desert Vigiles
	- Armoured Numidian Spearmen
	- Numidian Noble Infantry

- Missile Infantry: 4
	- Tribesmen
	- Tribal Slingers
	- Numidian Javelinmen
	- Heavy Numidian Skirmishers

- Melee Cavalry: 3
	- Desert Cavalry
	- Armoured Desert Cavalry
	- Desert Legionary Cavalry

- Shock Cavalry: 2
	- Numidian Riders
	- Armoured Numidian Riders

- Missile Cavalry: 4
	- Gaetuli Horse Skirmishers
	- Numidian Cavalry
	- Armoured Numidian Cavalry
	- Numidian Noble Cavalry

- Elephant: 1
	- African Elephants

- Chariot: 2
	- Desert Chariots
	- Armoured Desert Chariots

###	MASSAGETAE: 14 UNITS

- Melee Infantry: 1
	- Young Axes

- Spear Infantry: 2
	- Tribesmen
	- Steppe Spearmen

- Missile Infantry: 1
	- Steppe Archers

- Shock Cavalry: 5
	- Steppe Lancers
	- Steppe Armoured Lancers
	- Steppe Noble Lancers
	- Saka Noble Armoured Lancers
	- Saka Cataphracts

- Missile Cavalry: 5
	- Steppe Horse Skirmishers
	- Steppe Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers
	- Saka Cataphracts Horse Archers

###	MASSILIA: 19 UNITS

- Melee Infantry: 3
	- Celtic Warriors
	- Axe Warriors
	- Thorax Swordsmen

- Spear Infantry: 7
	- Celtic Tribesmen
	- Citizen Hoplites
	- Levy Freemen
	- Light Hoplites
	- Massalian Thureos Spears
	- Hoplites
	- Massalian Hoplites

- Missile Infantry: 3
	- Celtic Slingers
	- Celtic Skirmishers
	- Peltasts

- Melee Cavalry: 3
	- Light Horse
	- Citizen Cavalry
	- Massalian Cavalry

- Shock Cavalry: 1
	- Hippeus Lancers

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry

###	NABATEA: 28 UNITS

- Melee Infantry: 4
	- Desert Levy
	- Nabatean Swordsmen
	- Nabatean Axe Warriors
	- Noble Swordsmen

- Spear Infantry: 6
	- Levy Spearmen
	- Desert Hoplites
	- Caravan Guard
	- Armoured Desert Hoplites
	- Heavy Desert Spearmen
	- Rekem Palace Guard

- Pike Infantry: 2
	- Desert Pikemen
	- Nabatean Thorax Pikes

- Missile Infantry: 5
	- Tribes People
	- Slingers
	- Levy Skirmishers
	- Nabatean Light Peltasts
	- Nabatean Heavy Archers

- Melee Cavalry: 6
	- Camel Spearmen
	- Desert Cavalry
	- Arabian Cavalry
	- Armoured Desert Cavalry
	- Armoured Camel Spearmen
	- Nabatean Noble Cavalry

- Shock Cavalry: 2
	- Desert Heavy Lancers
	- Hellenic Desert Cataphracts

- Missile Cavalry: 1
	- Camel Archers

- Chariot: 2
	- Desert Chariots
	- Scythed Chariots

###	NERVII: 16 UNITS

- Melee Infantry: 5
	- Celtic Warriors
	- Guerrilla Swordsmen
	- Naked Warriors
	- Fierce Swords
	- Oathsworn

- Spear Infantry: 4
	- Celtic Tribesmen
	- Levy Freemen
	- Spear Brothers
	- Naked Spears

- Missile Infantry: 4
	- Celtic Youths
	- Celtic Slingers
	- Celtic Skirmishers
	- Gallic Hunters

- Melee Cavalry: 3
	- Light Horse
	- Mighty Horse
	- Noble Horse

### ODRYSIAN KINGDOM: 11 UNITS

- Melee Infantry: 2
	- Thracian Warriors
	- Thracian Nobles

- Spear Infantry: 1
	- Spears

- Missile Infantry: 5
	- Tribal Garrison
	- Thracian Slingers
	- Thracian Bowmen
	- Thracian Skirmishers
	- Thracian Peltasts

- Melee Cavalry: 2
	- Thracian Horsemen
	- Thracian Royal Cavalry

- Missile Cavalry: 1
	- Thracian Cavalry

### PARTHIA: 22 UNITS

- Melee Infantry: 3
	- Mob
	- Hillmen
	- Parthian Swordsmen

- Spear Infantry: 2
	- Eastern Spearmen
	- Persian Hoplites

- Missile Infantry: 5
	- Eastern Javelinmen
	- Eastern Slingers
	- Persian Light Archers
	- Parthian Foot Archers
	- Elite Persian Archers

- Melee Cavalry: 3
	- Camel Spearmen
	- Median Cavalry
	- Noble Blood Cavalry

- Shock Cavalry: 3
	- Eastern Cataphracts
	- Camel Cataphracts
	- Royal Cataphracts

- Missile Cavalry: 5
	- Horse Skirmishers
	- Camel Archers
	- Parthian Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers

- Elephant: 1
	- Indian War Elephants

### PERGAMON: 20 UNITS

- Melee Infantry: 2
	- Mob
	- Galatian Swords

- Spear Infantry: 5
	- Militia Hoplites
	- Thureos Spears
	- Galatian Spears
	- Hoplites
	- Agema Spears

- Pike Infantry: 2
	- Levy Pikemen
	- Pikemen

- Missile Infantry: 5 (6)
	- Javelinmen
	- Slingers
	- Archers
	- (Light) Peltasts
	- Picked Peltasts

- Melee Cavalry: 2
	- Light Horse
	- Citizen Cavalry

- Shock Cavalry: 2
	- Hippeus Lancers
	- Pergamenes Noble Cavalry

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry

### PONTUS: 19 UNITS

- Melee Infantry: 3
	- Mob
	- Hillmen
	- Pontic Swordsmen

- Spear Infantry: 3
	- Eastern Spearmen
	- Thureos Spears
	- Hoplites

- Pike Infantry: 3
	- Levy Pikemen
	- Pikemen
	- Bronze Shield Pikemen

- Missile Infantry: 4
	- Eastern Javelinmen
	- Eastern Slingers
	- Eastern Archers
	- Pontic Peltasts

- Melee Cavalry: 3
	- Citizen Cavalry
	- Noble Blood Cavalry
	- Cappadocian Cavalry

- Shock Cavalry: 1
	- Pontic Royal Cavalry

- Missile Cavalry: 1
	- Horse Skirmishers

- Chariot: 1
	- Scythed Chariots

### ROME (NO AUX): 22 UNITS

- Melee Infantry: 10 (16)
	- Plebs
	- Socii Hastati
	- Socii Extraordinarii
	- Gladiators
	- Gladiatrices
	- Hastati
	- Principes
	- Legionaries (Hastati, Principes)
	- Veteran Legionaries (Triarii)
	- First Cohort
	- Legionary Cohort (Legionaries)
	- Evocati Cohort (Veteran Legionaries)
	- Eagle Cohort (First Cohort)
	- Armoured Legionaries
	- Praetorians
	- Praetorian Guard (Praetorians)

- Spear Infantry: 5 (6)
	- Rorarii
	- Auxiliary Infantry
	- Vigiles (Rorarii)
	- Spear Gladiators
	- Spear Gladiatrices
	- Triarii

- Missile Infantry: 2
	- Leves
	- Velites

- Melee Cavalry: 4 (5) 
	- Equites
	- Socii Equites
	- Legionary Cavalry (Equites)
	- Auxiliary Cavalry
	- Praetorian Cavalry

- Shock Cavalry: 1
	- Socii Equites Extraordinarii

###	ROXOLANI: 13 UNITS

- Melee Infantry: 1
	- Young Axes

- Spear Infantry: 2
	- Tribesmen
	- Steppe Spearmen

- Missile Infantry: 1
	- Steppe Archers

- Melee Cavalry: 2
	- Sarmatian Riders
	- Sarmatian Horsemen

- Shock Cavalry: 4
	- Steppe Lancers
	- Steppe Armoured Lancers
	- Steppe Noble Lancers
	- Sarmatian Royal Lancers

- Missile Cavalry: 3
	- Steppe Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers

### ROYAL SCYTHIA: 15 UNITS

- Melee Infantry: 1
	- Young Axes

- Spear Infantry: 2
	- Tribesmen
	- Steppe Spearmen

- Missile Infantry: 1
	- Steppe Archers

- Melee Cavalry: 1
	- Scythian Royal Horse

- Shock Cavalry: 2
	- Steppe Armoured Lancers
	- Steppe Noble Lancers

- Missile Cavalry: 8
	- Steppe Horse Skirmishers
	- Steppe Horse Archers
	- Scythian 'Amazonian' Riders
	- Armoured Horse Archers
	- Scythian Noblewomen
	- Noble Horse Archers
	- Royal Horse Archers
	- Scythian Royal Skirmishers

### SABA: 25 UNITS

- Melee Infantry: 4
	- Desert Levy
	- Sabaean Swordsmen
	- Mercenary Maas Gat Marauders
	- Noble Swordsmen

- Spear Infantry: 5
	- Desert Spearmen
	- Sabaean Spearmen
	- Caravan Guard
	- Mercenary Ma'rib Guard
	- Ma'rib Royal Guard

- Missile Infantry: 4
	- Tribes People
	- Slingers
	- Levy Skirmishers
	- Sabaean Archers

- Melee Cavalry: 6
	- Camel Spearmen
	- Desert Cavalry
	- Arabian Cavalry
	- Mercenary Himyar Cavalry
	- Armoured Camel Spearmen
	- Royal Ma'rib Cavalry

- Shock Cavalry: 4
	- Camel Lancers
	- Desert Heavy Lancers
	- Sabaean Camel Cataphracts
	- Ma'rib Camel Cataphracts

- Missile Cavalry: 2
	- Camel Archers
	- Royal Camel Archers

- Chariot: 1
	- Desert Chariots
	
### SELEUCIDS: 34 UNITS

- Melee Infantry: 5
	- Mob
	- Hillmen
	- Thorax Swordsmen
	- Silver Shield Swordsmen
	- Royal Peltasts

- Spear Infantry: 4
	- Eastern Spearmen
	- Persian Hoplites
	- Thureos Spears
	- Shield Bearers

- Pike Infantry: 4
	- Levy Pikemen
	- Pikemen
	- Thorax Pikemen
	- Silver Shield Pikemen

- Missile Infantry: 7 (8)
	- Javelinmen
	- Eastern Javelinmen
	- Eastern Slingers
	- Archers
	- Persian Light Archers
	- (Light) Peltasts
	- Syrian Heavy Archers

- Melee Cavalry: 5
	- Camel Spearmen
	- Light Cavalry
	- Citizen Cavalry
	- Median Cavalry
	- Azat Knights

- Shock Cavalry: 2
	- Agema Cavalry
	- Hellenic Cataphracts

- Missile Cavalry: 4
	- Horse Skirmishers
	- Skirmisher Cavalry
	- Camel Archers
	- Tarantine Cavalry

- Elephant: 2
	- Indian War Elephants
	- Indian Armoured Elephants

- Chariot: 1
	- Scythed Chariots

### SPARTA: 16 UNITS

- Spear Infantry: 6
	- Spartan Youths
	- Periokoi Spears
	- Periokoi Hoplites
	- Spartan Hoplites
	- Royal Spartans
	- Heroes of Sparta

- Pike Infantry: 2
	- Periokoi Pikemen
	- Spartan Pikemen

- Missile Infantry: 5
	- Helot Javelinmen
	- Helot Slingers
	- Helot Archers
	- Gorgo's Skirmishers
	- Periokoi Peltasts

- Melee Cavalry: 1
	- Citizen Cavalry

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry

###  SUEBI: 22 UNITS

- Melee Infantry: 7
	- Club Levy
	- Hex-Bearers
	- Bloodsworm
	- Round Shield Swordsmen
	- Wolf Warriors
	- Berserkers
	- Sword Masters

- Spear Infantry: 7
	- Germanic Tribesmen
	- Spear Levy
	- Spear Brothers
	- Spearwomen
	- Spear Wall
	- Night Hunters
	- Wodanaz Spears

- Missile Infantry: 5
	- Germanic Slingers
	- Germanic Youths
	- Horse Runners
	- Longbow Hunters
	- Cimbri Bow-Women

- Melee Cavalry: 2
	- Riders of the Hunt
	- Noble Riders

- Missile Cavalry: 1
	- Germanic Scout Riders

### SYRACUSE: 19 UNITS

- Melee Infantry: 2
	- Mob
	- Thorax Swordsmen

- Spear Infantry: 8
	- Citizen Hoplites
	- Militia Hoplites
	- Light Hoplites
	- Thureos Spears
	- Thureos Hoplites
	- Hoplites
	- Thorax Hoplites
	- Picked Hoplites

- Pike Infantry: 1
	- Pikemen

- Missile Infantry: 4 (5)
	- Javelinmen
	- Slingers
	- Archers
	- (Light) Peltasts

- Melee Cavalry: 1
	- Citizen Cavalry

- Shock Cavalry: 1
	- Hippeus Lancers

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry

### TYLIS: 13 UNITS

- Melee Infantry: 5
	- Celtic Warriors
	- Thracian Warriors
	- Galo-Thracian Infantry
	- Tribal Warriors
	- Oathsworn

- Spear Infantry: 3
	- Celtic Tribesmen
	- Levy Freemen
	- Spear Warriors

- Missile Infantry: 2
	- Celtic Slingers
	- Celtic Skirmishers

- Melee Cavalry: 2
	- Light Horse
	- Noble Horse

- Missile Cavalry: 1
	- Raiding Horsemen